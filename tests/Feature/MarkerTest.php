<?php

it('can add e marker', function() {
   $response = $this->postJson(route('add-marker'), [
       'latitude' => fake()->latitude,
       'longitude' => fake()->longitude,
       'title' => fake()->streetAddress
   ]);

   $response->assertStatus(201);
});

it('get markers', function() {
    \App\Models\Marker::factory(10)->create();
    $response = $this->getJson(route('get-markers'));
    $response->assertStatus(200);

});

it('remove marker', function() {
    \Illuminate\Support\Facades\Queue::fake();
    $response = $this->postJson(route('add-marker'), [
        'latitude' => fake()->latitude,
        'longitude' => fake()->longitude,
        'title' => fake()->streetAddress
    ]);

    \Illuminate\Support\Facades\Queue::assertPushed(\App\Jobs\RemoveMarkerJob::class);
});
