<?php

it('can remove marker', function() {
   $markerService = new \App\Services\MarkerService();

   $markers = \App\Models\Marker::factory(15)->create();

   $markers->first()->update(['created_at' => now()->subMinutes(2)]);
   $markerService->removeMarker($markers->first()->id);
   $this->assertEquals(14, \App\Models\Marker::count());
});

it('can add marker', function() {
    $markerService = new \App\Services\MarkerService();

    $markerService->addMarker([
        'title' => fake()->streetAddress,
        'latitude' => fake()->latitude,
        'longitude' => fake()->longitude
    ]);

    $this->assertEquals(1, \App\Models\Marker::count());
});
