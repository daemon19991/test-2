import './bootstrap';

let map;
// let markerList;
getMarkers();
setupMapMarkerChannel();


async function initMap(markerList) {
    if (markerList) {
        const {Map} = await google.maps.importLibrary("maps");

        // The location of Uluru
        const uluru = {lat: -25.344, lng: 131.031};
        // The map, centered at Uluru
        const map = new Map(document.getElementById("map"), {
            zoom: 4,
            center: uluru,
        });
        // The marker, positioned at Uluru
        const infoWindow = new google.maps.InfoWindow();

        map.addListener("click", (mapsMouseEvent) => {
            getCoordinates(mapsMouseEvent.latLng.toJSON());
        });

        Array.from(markerList['data']).forEach((element) => {
            let marker = new google.maps.Marker({
                position: {lat: +element.latitude, lng: +element.longitude},
                map: map,
                title: element.title,
            });

            marker.addListener("click", () => {

                infoWindow.close();
                infoWindow.setContent(marker.title);
                infoWindow.open(marker.map, marker);
            });
        });
    }
}



let addMarker = document.getElementById('add-marker');

addMarker.addEventListener("click", (e) => {
    addUserMarker(e);
});

function getCoordinates(coordinates) {
    let latitude = document.getElementById('latitude');
    let longitude = document.getElementById('longitude');

    latitude.value = coordinates['lat'];
    longitude.value = coordinates['lng'];
    console.log('coordinates', coordinates['lat']);
}

function addUserMarker(e) {
    e.preventDefault();
    if (validation()) {
        let title = document.getElementById('title');
        let latitude = document.getElementById('latitude');
        let longitude = document.getElementById('longitude');

        console.log('22add-marker');
        const xhr = new XMLHttpRequest();
        xhr.open("POST", "/api/add-marker");
        xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8")

        const body = JSON.stringify({
            title: title.value,
            longitude: longitude.value,
            latitude: latitude.value
        });
        xhr.onload = () => {
            if (xhr.readyState == 4 && xhr.status == 201) {
                console.log(JSON.parse(xhr.responseText));
                getMarkers();
            } else {
                console.log(`Error: ${xhr.status}`);
            }

            let error = document.getElementById('error');

            error.style.display = 'none';
        };
        xhr.send(body);
    }

    return false;
}

function getMarkers() {
    let markerList;
    console.log('get markers');
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "/api/get-markers");
    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8")

    xhr.onload = () => {
        console.log('tutu');
        if (xhr.readyState == 4 && xhr.status == 200) {
            markerList = JSON.parse(xhr.responseText);
            initMap(markerList);

            clearForm();

            console.log(markerList);
        } else {
            console.log(`Error: ${xhr.status}`);
        }

        let error = document.getElementById('error');

        error.style.display = 'none';
    };

    xhr.send();
}

function validation() {
    let fields = document.getElementById('form-add-marker').elements;
    let error = document.getElementById('error');
    let showError = false;

    Array.from(fields).forEach((field) => {
        if (field.type === 'text' && field.value == '') {
            showError = true;
        }
    });

    if (showError === true) {
        error.style.display = 'block';
    }
    return !showError;
}

function setupMapMarkerChannel() {
    Echo.channel('map')
        .listen('.AddMarker', (e) => {
            console.log('added Marker', e);
            getMarkers();
        });
};

function clearForm() {
    let fields = document.getElementById('form-add-marker').elements;

    Array.from(fields).forEach((field) => {
        if (field.type === 'text') {
            field.value = '';
        }
    });
}

