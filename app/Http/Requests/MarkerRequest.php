<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class MarkerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'longitude' => 'required',
            'latitude' => 'required'
        ];
    }


//    public function messages()
//    {
//        return array_merge(parent::messages(), [
//            'title.unique' => 'Sorry, there is already a user with this phone number. Please log in.',
//            'longitude.unique' => 'This email has already been taken',
//            'latitude.unique' => 'This email has already been taken',
//        ]);
//    }
}
