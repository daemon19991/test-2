<?php

namespace App\Http\Controllers;

use App\Http\Requests\MarkerRequest;
use App\Services\MarkerService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Knuckles\Scribe\Attributes\Endpoint;

class MarkerController extends Controller
{
    /** @var MarkerService */
    private $markerService;

    public function __construct(MarkerService $markerService)
    {
        $this->markerService = $markerService;
    }

    #[Endpoint("Add new marker")]
    public function add(MarkerRequest $request) {
        return $this->markerService->addMarker($request->validated());
    }

    #[Endpoint("Get marker list")]
    public function getList() {
        return $this->markerService->getList();
    }
}
