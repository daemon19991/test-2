<?php

namespace App\Services;

use App\Events\AddMarker;
use App\Http\Requests\MarkerRequest;
use App\Http\Resources\MarkerCollection;
use App\Http\Resources\MarkerResource;
use App\Jobs\RemoveMarkerJob;
use App\Models\Marker;
use Illuminate\Http\Request;

class MarkerService
{
    public function addMarker($data)
    {
        $marker = Marker::create($data);
        $job = new RemoveMarkerJob($marker);
        $job->dispatch($marker->id)->delay(now()->addMinutes(1));

        event(new AddMarker());
        return new MarkerResource($marker);
    }

    public function getList()
    {
        return new MarkerCollection(Marker::all());
    }

    public function removeMarker($markerId)
    {
        return Marker::findOrFail($markerId)->delete();
    }
}
