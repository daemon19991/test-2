<?php

namespace App\Jobs;

use App\Events\AddMarker;
use App\Models\Marker;
use App\Services\MarkerService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RemoveMarkerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $markerId;
    private MarkerService $markerService;

    /**
     * Create a new job instance.
     */
    public function __construct($markerId)
    {
        $this->markerId = $markerId;
        $this->markerService = new MarkerService();
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->markerService->removeMarker($this->markerId);

        event(new AddMarker());
    }
}
