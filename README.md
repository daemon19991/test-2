## About project
This project is a test task, where users can add location markers on the map, and all users can see all markers using websockets.

## Links
API Doc: YOUR_DOMAIN/docs/
Requirements: https://docs.google.com/document/d/1AsY-manHg2SEeFeoO02jxVHyYloz4EAbk8-mDL4x9Cc/edit?usp=sharing

## 3ds party services

- Pusher: https://pusher.com
- Google MAP API

## Requirements

- PHP: 8.1
- MySQL: 8.0
- Node: 18.*

## Main init

```bash
composer install
```

```bash
npm install
```

#### Database
Need create two databases. Main and for tests, then need in .env file and phpunit.xml write needed db settings

#### Broadcast
Pusher uses for broadcasting (websocket) on server. You need register your account and take needed pusher params

BROADCAST_DRIVER=pusher

PUSHER_APP_ID=YOUR_APP_IP

PUSHER_APP_KEY=YOUR_APP_KEY

PUSHER_APP_SECRET=YOUR_APP_SECRET

PUSHER_APP_CLUSTER=YOUR_APP_CLUSTER

#### QUEUE
In .env file you need set queue connection

QUEUE_CONNECTION=database

#### Styles and Scripts
You need to run
```bash
npm build
```

## Testing
If you set in phpunit.xml needed db test params, for start testing you need use this command: 
```bash
./vendor/bin/pest
```

## DOC
For generating new doc you need use this command:

```bash
 php artisan scribe:generate
 ```

After you can visit docs page: 

YOUR_DOMAIN/docs/
