<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MarkerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::namespace('Marker')
    ->middleware(['api', 'response.json'])
    ->group(function () {
        Route::post('/add-marker', [MarkerController::class, 'add'])->name('add-marker');
        Route::get('/get-markers', [MarkerController::class, 'getList'])->name('get-markers');
    });

